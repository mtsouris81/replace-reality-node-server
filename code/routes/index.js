const express = Inject.get('express');
const bodyParser = Inject.get('body-parser');
const context = Inject.get('app-context');

let returnPage = function (res) {
    res.render('home', { name: context.state.topTitle, msg: context.state.message });
}

let urlencodedParser = bodyParser.urlencoded({ extended: false });
let jsonParser = bodyParser.json();
let router = express.Router();



/* GET home page. */
router.get('/', function(req, res, next) {
    returnPage(res);
});

/* GET routed page "whatever" */
router.get('/whatever', function(req, res, next) {
    context.state.topTitle = "dude";
    returnPage(res);
});

/* GET routed page "whatever-to/[any value passed as param]" */
router.get('/whatever-to/:someinput', function (req, res, next) {
    context.state.topTitle = req.params.someinput;
    returnPage(res);
});

/* POST	 routed page "whatever" with standard FORM encoded data */
router.post('/whatever', urlencodedParser, function(req, res, next) {
    context.state.topTitle = req.body.userinput;
    returnPage(res);
});

/* POST	 routed page "givemejson" with JSON data */
router.post('/givemejson', jsonParser, function(req, res, next) {
    context.state.topTitle = 'you posted JSON: ' +  req.body.userinput;
    returnPage(res);
});

module.exports = router;
