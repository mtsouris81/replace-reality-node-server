﻿let _socketServer = {};
_socketServer.create = function (port) {
    const WebSocket = require('ws');
    let wss = new WebSocket.Server({ port: port });

    wss.on('connection',
        function connection(ws) {
            ws.on('message', function incoming(message) {
                console.log('received: %s', message);
                ws.send(message);
            });
            ws.send('something');
        });

    wss.on('message',
        function connection(ws) {
            ws.on('message', function incoming(message) {
                console.log('received: %s', message);
            });
            ws.send('something');
        });

    return wss;
};

exports.hubManager = _socketServer;


