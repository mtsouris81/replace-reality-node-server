// container
global._webRoot = __dirname;
var _inject =
{
    get: function (typeName) {
        try {
            return require(typeName);
        } catch {
            return require(_webRoot + '/modules/' + typeName.toLowerCase());
        }
    },
    router: function (typeName) {
        try {
            return require(typeName);
        } catch {
            return require(_webRoot + '/routes/' + typeName.toLowerCase());
        }
    }
};
global.Inject = _inject;





const createError = Inject.get('http-errors');
const express = Inject.get('express');
const path = Inject.get('path');
const cookieParser = Inject.get('cookie-parser');
const logger = Inject.get('morgan');
const exphbs = Inject.get('express-handlebars');
const context = Inject.get('app-context');
const sockets = Inject.get('hub-server');

const indexRouter = Inject.router('index');
const usersRouter = Inject.router('users');


const app = express();

app.engine('.hbs', exphbs({
    defaultLayout: 'main',
    extname: '.hbs',
    layoutsDir: path.join(__dirname, 'views/layouts')
}));

app.set('view engine', '.hbs');
app.set('views', path.join(__dirname, 'views'));


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));



// socket
const webSocketServer = sockets.hubManager.create(3001);

// ROUTING
app.use('/', indexRouter);
app.use('/users', usersRouter);



// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


module.exports = app;





